import React, { Component } from "react";
import Home from "./pages/Home";
import "./App.css";
import "bulma/css/bulma.css";
import Detail from "./pages/Detail";

class App extends Component {
  render() {
    // especie de enrutador, creado a partir de la direccion actual de la pagina
    const url = new URL(document.location);
    const Page = url.searchParams.has("id") ? (
      <Detail id={url.searchParams.get("id")}></Detail>
    ) : (
      <Home />
    );
    return <div className="App">{Page}</div>;
  }
}

export default App;
