import React, { Component } from "react";
import { Title } from "../components/Title";
import { SearchForm } from "../components/SearchForm";
import MoviesList from "../components/MoviesList";

export default class Home extends Component {
  state = {
    results: [],
    usedSearch: false,
  };

  handleResults = (results) => {
    this.setState({ results, usedSearch: true });
  };

  renderResults = () => {
    return this.state.results.length === 0 ? (
      <p>Sorry! Results no found!</p>
    ) : (
      <MoviesList movies={this.state.results}></MoviesList>
    );
  };

  render() {
    return (
      <div className="home">
        <Title>Movie Searcher</Title>
        <div className="searchForm-wrapper">
          <div className="searchForm-wrapper">
            <SearchForm onResults={this.handleResults}></SearchForm>
          </div>
        </div>
        {this.state.usedSearch ? (
          this.renderResults()
        ) : (
          <small>Use the form to search a movie</small>
        )}
      </div>
    );
  }
}
