import React, { Component } from "react";
// import { useHistory } from "react-router";
import Proptypes from "prop-types";
import './pages.css';
const API_KEY = "708c8875";


export default class Detail extends Component {
  constructor(props) {
    super(props);
    // this.history = useHistory();
  }

  static propTypes = {
    id: Proptypes.string,
  };

  state = {
    movie: {},
  };

  fetchMovie = ({ id }) => {
    fetch(`http://www.omdbapi.com/?apikey=${API_KEY}&i=${id}`)
      .then((res) => res.json())
      .then((movie) => {
        console.log({ movie });
        // si los datos que vienen estan indefinidos se les asigna un valor por defecto
        this.setState({ movie });
      });
  };

  goback() {
    // esto sirve como ejemplo no mas, no para utilizarlo en prod
    window.history.back();
    // this.history.goBack();
  }

  // el compoenente ya se ha renderizado al menos una vez
  componentDidMount() {
    //  utilizaremos la id para llamar a un metodo fetch movie
    const { id } = this.props;
    this.fetchMovie({ id });
  }
  render() {
    const { Title, Poster, Actors, Metascore, Plot } = this.state.movie;

    return (
      <div>
        <button onClick={this.goback} className="goHomeButton">Go home</button>
        <div className="movieDetailContent">
          <h1 className="movieDetailTitle">{Title}</h1>
          <figure className="movieDetailPosterContainer">
            <img src={Poster} alt="poster" className="movieDetailPoster" />
          </figure>
          <h3 className="movieDetailActors">{Actors}</h3>
          <span className="movieDetailMetascore">Metascore: {Metascore}</span>
          <p className="movieDetailPlot">{Plot}</p>
        </div>
      </div>

    );
  }
}
