import React, { Component } from "react";
import Movie from "./Movie";
import Proptype from "prop-types";
import "./components.css";

export default class MoviesList extends Component {
  static propType = {
    movie: Proptype.array,
  };

  render() {
    const { movies } = this.props;

    return (
      <div className="moviesList">
        {movies.map((movie) => {
          return (
            <div className="moviesListItem" key={movie.imdbID}>
              <Movie
                id={movie.imdbID}
                title={movie.Title}
                year={movie.Year}
                poster={movie.Poster}
              >
                {movie.Title}
              </Movie>
            </div>
          );
        })}
      </div>
    );
  }
}
