import React from "react";

export const Title = ({ children }) => {
  return <h1 className="title">{children}</h1>;
};

// exportar de forma nombrada= export const nombreComponente
// exportar default puede ser importado con cualquier nombre: export default ({ children }) => {
