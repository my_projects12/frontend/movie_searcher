import React, { Component } from "react";
import Proptypes from "prop-types";
import "./components.css";

class Movie extends Component {
  static propType = {
    id: Proptypes.string,
    title: Proptypes.string,
    year: Proptypes.string,
    poster: Proptypes.string,
  };

  // sirve para forzar una relación de aspecto en una imagen
  // esto: is-4by3, (es una clase)

  render() {
    const { id, poster, title, year } = this.props;
    return (
      <a href={`?id=${id}`} className="card">
        <div className="card-image">
          <img src={poster} alt={title} className="posterImage" />
        </div>
        <div className="card-content">
          <div className="media">
            <div className="media-content">
              <p className="title is-4">{title}</p>
              <p className="subtitle is-6">{year}</p>
            </div>
          </div>
        </div>
      </a>
    );
  }
}

export default Movie;
