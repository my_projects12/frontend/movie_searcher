import React, { Component } from "react";

const API_KEY = "708c8875";
export class SearchForm extends Component {
  state = {
    inputMovie: "",
  };

  _handleChange = (e) => {
    this.setState({ inputMovie: e.target.value });
  };

  _handleSubmit = (e) => {
    // para evitar que se ejecute el evento nativo por defecto que realiza el navegador cuando enviamos el form
    e.preventDefault();
    const { inputMovie } = this.state;
    // el metodo fetch devuelve una promesa
    fetch(`http://www.omdbapi.com/?apikey=${API_KEY}&s=${inputMovie}`)
      .then((res) => res.json())
      .then((results) => {
        // si los datos que vienen estan indefinidos se les asigna un valor por defecto
        const { Search = [], totalResults = "0" } = results;
        this.props.onResults(Search);
      });
  };

  render() {
    return (
      <form action="" onSubmit={this._handleSubmit}>
        <div className="field has-addons">
          <div className="control">
            <input
              className="input"
              onChange={this._handleChange}
              placeholder="Movie to search..."
              type="text"
              value={this.state.inputMovie}
            />
          </div>
          <div className="control">
            <button className="button is-info" href="!#">
              Search
            </button>
          </div>
        </div>
      </form>
    );
  }
}


